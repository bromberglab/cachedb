FROM python:3.7-alpine as base

LABEL bio_node=v1.0 \
      bionode_entrypoint="python -m bionode" \
      input_1="fasta_file|fasta_gz|folder" \
      input_2="ppcache_db" \
      output="ppcache_result" \
      maintainer=mmiller@bromberglab.org \
      description="mi-faser docker image (https://services.bromberglab.org/mifaser)"

FROM base as builder

# setup system
RUN mkdir /install
WORKDIR /install
RUN apk update && apk add alpine-sdk && rm -rf /var/cache/apk/*

# setup app
COPY . /app
RUN pip install --upgrade pip && pip install --prefix=/install -r /app/requirements.txt

FROM base

COPY --from=builder /install /usr/local
COPY --from=builder /app /app

# set environment variables
WORKDIR /app
ENV INPUT_PATH=/input OUTPUT_PATH=/output

# set project ENTRYPOINT
ENTRYPOINT ["python", "-m", "ppcache"]

# set project CMD
# CMD []
