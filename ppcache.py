import argparse
import tarfile
import logging
import tempfile
from pathlib import Path
from shutil import copytree
from Bio.SeqIO import read
from Bio.SeqUtils.CheckSum import seguid


class PPfilecache:
    def __init__(self, query_in:Path, query_format='fasta', storage_path:Path=Path.cwd(), prefix='query'):
        self.log = logging.getLogger('PPfilecache')
        self.query_in = query_in
        self.storage = storage_path
        self.prefix = prefix
        self.record = read(query_in, query_format)
        self.seguid = seguid(str(self.record.seq))
        self.seguid_hex = self.seguid.encode().hex()
        self.seguid_path = self.storage / self.seguid_hex

    def store(self):
        pass
    
    def lookup(self):
        if self.seguid_path.exists():
            seguid_hex_fasta = self.seguid_path / f'{self.prefix}.in'
            if not seguid_hex_fasta.exists():
                self.log.info(f'No PredictProteinCache available: {self.record.id} [{self.seguid_path}]')
                return False
            record = read(seguid_hex_fasta, 'fasta')
            assert(self.record.seq == record.seq)
            self.log.info(f'Found PredictProteinCache: {self.record.id} [{self.seguid_path}]')
            return True
        self.log.info(f'No PredictProteinCache available: {self.record.id} [{self.seguid_path}]')
        return False

def convert(cache_in:Path, storage_path:Path=Path.cwd(), prefix='query'):
    log = logging.getLogger('PPfilecache.convert')
    if cache_in.suffix == '.gz':
        temp_extract = tempfile.TemporaryDirectory()
        tf = tarfile.open(cache_in)
        tf.extractall(temp_extract.name)
        extracted_path = Path(temp_extract.name)
        extracted_content = Path(temp_extract.name).glob('*')
        dirs = [x for x in extracted_content if x.is_dir()]
        if dirs:
            if len(dirs) == 1:
                cache_in = dirs[0]
            else:
                log.warn(f'Found multiple cache entries in: {cache_in} [{dirs}]. Skipping conversion.')
                return None
        else:
            files = [x for x in extracted_content if x.is_file()]
            if files:
                cache_in = extracted_path
            else:
                log.error(f'Could not find any valid cache entries in: {cache_in}. Skipping conversion.')
                return None
    query_fasta = cache_in / f'{prefix}.in'
    ppcache = PPfilecache(query_fasta, storage_path=storage_path, prefix=prefix)
    if ppcache.lookup():
        log.warn(f'PredictProteinCache entry already exists: {ppcache.record.id} [{ppcache.seguid_path}]')
    else:
        log.info(f'Converting to PredictProteinCache: {ppcache.record.id} [{ppcache.seguid_path}]')
        copytree(cache_in, ppcache.seguid_path)
    return ppcache.seguid


    def bionode_run():
        raise NotImplementedError()

    
    def bionode_run_file():
        raise NotImplementedError()


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument('fasta', nargs='*', help='input fasta file(s)')
    parser.add_argument('-c', '--cachein', help='existing cache folder or tar.gz archive')
    parser.add_argument('-C', '--cachestorage', default=Path.cwd())
    parser.add_argument('-l', '--log', default="INFO")
    args = parser.parse_args()

    logging.basicConfig(level=args.log.upper())

    if args.cachein:
        convert(cache_in=Path(args.cachein), storage_path=Path(args.cachestorage))
